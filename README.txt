ONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Module Maker module allows the user to create a module with default files.

 * For a full description of the module visit:
   https://www.drupal.org/project/module_maker

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/module_maker


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the User Module Maker module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Module Maker.


MAINTAINERS
-----------

8.x-1.x Developed and maintained by:
 * Kishan T (https://www.drupal.org/user/3589556)
 
